package com.gallery;

public class Data {

	String albumId, id, title, url, thumbnailUrl;

	Data(String albumId, String id, String title, String url,
			String thumbnailUrl) {
		this.albumId = albumId;
		this.id = id;
		this.title = title;
		this.url = url;
		this.thumbnailUrl = thumbnailUrl;
	}
}