package com.gallery;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.TextHttpResponseHandler;
import com.utilitypackage.ResponseSaver;
import com.utilitypackage.ServerHttpClientClass;
import com.utilitypackage.UtillClass;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

public class MainActivity extends Activity {

	private ResponseSaver responseSaver;
	private RecyclerView mRecyclerView;
	private RecyclerView.LayoutManager mLayoutManager;
	@SuppressWarnings("rawtypes")
	private RecyclerView.Adapter mAdapter;
	private ArrayList<Data> dataList = new ArrayList<Data>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
		mRecyclerView.setHasFixedSize(true);

		mLayoutManager = new LinearLayoutManager(this);
		mRecyclerView.setLayoutManager(mLayoutManager);

		responseSaver = new ResponseSaver(MainActivity.this);
		// If response saved in local storage then we can get it from shared
		// preference and no need to connect server again.
		if (responseSaver.isResponseSaved()) {
			parseData(responseSaver.getSavedResponse());
		} else { // If first time open the application, we call the server for
					// getting response and save in local storage.

			// checking internet connection.
			if (UtillClass.isConnectingToInternet(MainActivity.this)) {
				final ProgressDialog dialog = new ProgressDialog(
						MainActivity.this);
				ServerHttpClientClass.get(
						"http://jsonplaceholder.typicode.com/photos",
						new TextHttpResponseHandler() {

							@Override
							public void onStart() {
								super.onStart();
								dialog.setMessage(getResources().getString(
										R.string.prog_loading));
								dialog.setCancelable(false);
								dialog.show();
							}

							@Override
							public void onSuccess(int arg0, Header[] arg1,
									String response) {
								// save the response in local storage
								responseSaver.saveResponse(response);
								// call the parsing method to parse the JSON
								// response
								// and populate images in list using Adapter.
								parseData(response);
							}

							@Override
							public void onFinish() {
								super.onFinish();
								dialog.dismiss();
							}

							@Override
							public void onFailure(int arg0, Header[] arg1,
									String failureMessage, Throwable arg3) {
								dialog.dismiss();
								Log.e("server connecting onFailure", ""
										+ failureMessage);
							}
						});
			} else {
				Toast.makeText(MainActivity.this,
						getResources().getString(R.string.no_net),
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	protected void parseData(String response) {
		try {
			JSONArray array = new JSONArray(response);
			for (int i = 0; i < array.length(); i++) {
				JSONObject obj = array.getJSONObject(i);
				String albumId = "", id = "", title = "", url = "", thumbnailUrl = "";
				albumId = obj.getString("albumId");
				id = obj.getString("id");
				title = obj.getString("title");
				url = obj.getString("url");
				thumbnailUrl = obj.getString("thumbnailUrl");
				Data data = new Data(albumId, id, title, url, thumbnailUrl);
				dataList.add(data);
			}
			mAdapter = new Adapter(dataList);
			mRecyclerView.setAdapter(mAdapter);
		} catch (JSONException jsonException) {
			Log.e("parseData json exception", "" + jsonException.getMessage());
		} catch (Exception commonException) {
			Log.e("parseData common exception",
					"" + commonException.getMessage());
		}
	}

}