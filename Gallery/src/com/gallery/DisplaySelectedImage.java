package com.gallery;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

public class DisplaySelectedImage extends Activity {
	private ImageView selectedImg;
	private String selectedImgUrl;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.display_image);
		selectedImg = (ImageView) findViewById(R.id.selected_image);
//		Getting the URL of selected images from list view (list of images) from bundle in the Adapter class.
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
//			Getting actual image URL and loading using image loader library.
			selectedImgUrl = bundle.getString("selected_img_url");
//			Initialize image loader and set up display options.
			imageLoader = ImageLoader.getInstance();
			options = new DisplayImageOptions.Builder().cacheInMemory(true)
					.cacheOnDisc(true).resetViewBeforeLoading(true)
					.showImageForEmptyUri(R.drawable.placeholder)
					.showImageOnFail(R.drawable.placeholder)
					.showImageOnLoading(R.drawable.loading).build();
			imageLoader.displayImage(selectedImgUrl, selectedImg, options);
		}
	}
}