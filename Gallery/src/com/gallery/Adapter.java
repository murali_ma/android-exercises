package com.gallery;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class Adapter extends RecyclerView.Adapter<Adapter.HolderClass> {

	private ArrayList<Data> dataList = new ArrayList<Data>();
	private DisplayImageOptions options;
	private ImageLoader imageLoader;
	private Context context;

	class HolderClass extends RecyclerView.ViewHolder {
		public ImageView imgThumbnail;
		public TextView urlTextView;
		public View view;

		public HolderClass(View itemView) {
			super(itemView);
			imgThumbnail = (ImageView) itemView.findViewById(R.id.img_item);
			urlTextView = (TextView) itemView.findViewById(R.id.text_url);
			view = itemView;
//			Get full image URL and pass it Displayselectedimage class by using bundle.
			view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Bundle bundle = new Bundle();
					bundle.putString("selected_img_url", urlTextView.getText().toString());
					Intent intent = new Intent(context,
							DisplaySelectedImage.class);
					intent.putExtras(bundle);
					context.startActivity(intent);
				}
			});
		}
	}

	public Adapter(ArrayList<Data> dataList) {
		this.dataList = dataList;
	}

	@Override
	public int getItemCount() {
		return dataList.size();
	}

//	load each image in list from Data class using image loader.
	@Override
	public void onBindViewHolder(HolderClass viewHolder, int pos) {
		Data data = dataList.get(pos);
		viewHolder.urlTextView.setText(data.url);
		imageLoader.displayImage(data.thumbnailUrl, viewHolder.imgThumbnail,
				options);
	}

	@SuppressWarnings("deprecation")
	@Override
	public HolderClass onCreateViewHolder(ViewGroup viewgroup, int arg1) {
		View v = LayoutInflater.from(viewgroup.getContext()).inflate(
				R.layout.item, viewgroup, false);
		HolderClass view = new HolderClass(v);
		context = viewgroup.getContext();
//		Initialize image loader and set up display options.
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisc(true).resetViewBeforeLoading(true)
				.showImageForEmptyUri(R.drawable.placeholder)
				.showImageOnFail(R.drawable.placeholder)
				.showImageOnLoading(R.drawable.loading).build();
		return view;
	}
}