package com.utilitypackage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class ResponseSaver {

	private SharedPreferences pref;
	private Editor editor;
	private Context _context;
	private final String PREF_NAME = "response_info";
	private final String PREF_PARAM = "response";

	@SuppressLint("CommitPrefEdits")
	@SuppressWarnings("static-access")
	public ResponseSaver(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(PREF_NAME, _context.MODE_PRIVATE);
		editor = pref.edit();
	}

	public void saveResponse(String response) {
		editor.putString(PREF_PARAM, response);
		editor.commit();
	}

	public String getSavedResponse() {
		return pref.getString(PREF_PARAM, "");
	}

	public void clearSavedResponse() {
		editor.clear();
		editor.commit();
	}

	public boolean isResponseSaved() {
		return (pref.getString(PREF_PARAM, "").length() == 0 ? false : true);
	}
}